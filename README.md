GlusterFS
=========

Install GlusterFS 9 on Ubuntu

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

This role is only available via this git repo 

    - hosts: servers
      roles:
         - glusterfs

License
-------

Mozilla Public License 2.0

